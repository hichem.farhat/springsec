package tn.essat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Appsec1Application {

	public static void main(String[] args) {
		SpringApplication.run(Appsec1Application.class, args);
	}

}
